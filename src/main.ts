import { createApp } from 'vue'
import 'bulma/css/bulma.css'
import './styles.css'
// @ts-ignore
import App from './App.vue'

createApp(App).mount('#app')
